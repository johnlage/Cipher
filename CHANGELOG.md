Changelog
=========

0.3.2 (2018-03-24)
------------------

-   All: Fix PyPI

0.3.1 (2018-03-24)
------------------

-   Bridge: Rename CipherRelay to CipherBridge (more obvious name)
-   Bridge: Improve Format Loading/Selection
-   Bridge: Improve/Streamline Message Handling
-   Core: Improve Logging on Handler/Command Registration and Lookup
-   TG: Add Join/Leave Event Support
-   All: Implement UserSendMessageEvent (on all ConnectionTypes).
-   All: Implement better repr for all classes
-   All: Better Logging in General
-   All: Create Event.type from event\_type and event\_name
-   Packaging: Improve setup.py Metadeta, Move *.rst to *.md.

0.3.0 (2018-03-16)
------------------

-   Core: Log to File (configurable in the config)
-   Core: Better Configuration System
-   TG: Automatic Reconnects
-   All: Reorganize Modules into Packages
-   All: Improve setup.py/other packaging stuff.

0.2.1 (2017-12-28)
------------------

-   All: PM Support
-   IRC: Server Password Support
-   TG: Handle Replies/Forwards/etc

0.2.0 (2017-10-08)
------------------

-   Core: Command Line Interface
-   Core: Automatic Hastebin Functionality
-   Core: Basic Logging

0.1.0 (2017-06-11)
------------------

-   All: First release on git.
-   All: Add discord/IRC/TG support.
