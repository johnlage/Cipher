Overview
========

[![PyPI Package latest release](https://img.shields.io/pypi/v/Cipher-Bot.svg)](https://pypi.org/project/Cipher-Bot)
[![PyPI Wheel](https://img.shields.io/pypi/wheel/Cipher-Bot.svg)](https://pypi.org/project/Cipher-Bot)
[![Supported versions](https://img.shields.io/pypi/pyversions/Cipher-Bot.svg)](https://pypi.org/project/Cipher-Bot)
[![Supported implementations](https://img.shields.io/pypi/implementation/Cipher-Bot.svg)](https://pypi.org/project/Cipher-Bot)

A multi-platform chat bot framework.

-   Links: [Source](https://gitlab.com/johnlage/Cipher), [Issues](https://gitlab.com/johnlage/Cipher/issues)
-   Free software: Apache Software License 2.0

Installation
------------

```commandline
pip install Cipher-Bot
```
