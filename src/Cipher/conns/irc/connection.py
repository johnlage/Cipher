import asyncio

import pydle
import daiquiri

from Cipher.conns.irc.config import IRCConnectionConfig
from Cipher.conns.irc.event import IRCConnectingEvent, IRCDisconnectingEvent, IRCChannelSendMessageEvent, \
    IRCConnectedEvent, IRCUnexpectedDisconnectEvent, IRCDisconnectedEvent, IRCActionChannelEvent, IRCActionUserEvent, \
    IRCMessageChannelEvent, IRCMessageUserEvent, IRCJoinChannelEvent, IRCPartChannelEvent, IRCNoticeChannelEvent, \
    IRCNoticeUserEvent, IRCUserSendMessageEvent, IRCUserQuitEvent, IRCQuitChannelEvent
from Cipher.conns.irc.models import IRCUser, IRCChannel
from Cipher.core.connection import Connection


class IRCConnection(Connection):
    config_class = IRCConnectionConfig
    type = 'irc'
    multiline = False

    def __init__(self, core, conn_id, loop):
        super().__init__(core, conn_id, loop)
        # Initialize Instance Variables
        self.client = CipherClient(self)
        self.logger = daiquiri.getLogger(__name__)
        self.self_user = IRCUser(self.c.nickname, self.c.username, '?', self)  # Find way to get own hostname
        self.connected = False

    async def _connect(self):
        self.loop.create_task(self.client.connect())
        await self.core.handle_event(IRCConnectingEvent(self))

    async def _disconnect(self):
        await self.core.handle_event(IRCDisconnectingEvent(self))
        await self.client.disconnect()

    async def _send_message(self, target, message, source=''):
        await self.client.message(str(target), message)
        if isinstance(target, IRCChannel):
            await self.core.handle_event(IRCChannelSendMessageEvent(message, target, self.self_user, self, source))
        elif isinstance(target, IRCUser):
            await self.core.handle_event(IRCUserSendMessageEvent(message, target, self.self_user, self, source))

    def get_channel(self, name):
        return IRCChannel(name, self)

    def get_user(self, name):
        raise NotImplementedError

    def get_message_maxlen(self, target):
        return 500-len(str(target))

    def get_message_maxlines(self, target):
        return self.c.maxlines

    async def on_connect(self):
        """Register with IRC Server and join default channels."""

        await self.core.handle_event(IRCConnectedEvent(self))
        self.connected = True

    async def on_disconnect(self, expected):
        self.connected = False
        if expected:
            await self.core.handle_event(IRCDisconnectedEvent(self))
        else:
            await self.core.handle_event(IRCUnexpectedDisconnectEvent(self))

    async def on_message(self, target, sender, message):
        if sender == self.c.nickname:
            return
        if target == self.c.nickname:
            chan_msg = False
        else:
            chan_msg = True
        user = IRCUser(sender, self.client.users[sender]['username'], self.client.users[sender]['hostname'], self)
        if chan_msg:
            chan = IRCChannel(target, self)
            await self.core.handle_event(IRCMessageChannelEvent(message, chan, user, self))
        else:
            await self.core.handle_event(IRCMessageUserEvent(message, user, self))

    async def on_action(self, sender, target, message):
        if sender == self.c.nickname:
            return
        if target == self.c.nickname:
            chan_msg = False
        else:
            chan_msg = True
        user = IRCUser(sender, self.client.users[sender]['username'], self.client.users[sender]['hostname'], self)
        if chan_msg:
            chan = IRCChannel(target, self)
            await self.core.handle_event(IRCActionChannelEvent(message, chan, user, self))
        else:
            await self.core.handle_event(IRCActionUserEvent(message, user, self))

    async def on_join(self, channel, nick):
        if nick == self.c.nickname:
            return
        user = IRCUser(nick, self.client.users[nick]['username'], self.client.users[nick]['hostname'], self)
        chan = IRCChannel(channel, self)
        await self.core.handle_event(IRCJoinChannelEvent(chan, user, self))

    async def on_part(self, channel, nick, message):
        if nick == self.c.nickname:
            return
        if nick in self.client.users:
            user = IRCUser(nick, self.client.users[nick]['username'], self.client.users[nick]['hostname'], self)
        else:
            user = IRCUser(nick, 'unknownuser', 'host.unknown', self)
        chan = IRCChannel(channel, self)
        await self.core.handle_event(IRCPartChannelEvent(message, chan, user, self))

    async def on_quit(self, nick, message):
        if nick == self.c.nickname:
            return
        user = IRCUser(nick, self.client.users[nick]['username'], self.client.users[nick]['hostname'], self)
        await self.core.handle_event(IRCUserQuitEvent(message, user, self))
        for channel in [c for c in self.client.channels if nick in self.client.channels[c]['users']]:
            channel = IRCChannel(channel, self)
            await self.core.handle_event(IRCQuitChannelEvent(message, channel, user, self))

    async def on_notice(self, target, sender, message):
        if sender == self.c.nickname:
            return
        if target == self.c.nickname:
            chan_msg = False
        else:
            chan_msg = True
        if sender not in self.client.users:
            return
        user = IRCUser(sender, self.client.users[sender]['username'], self.client.users[sender]['hostname'], self)
        if chan_msg:
            chan = IRCChannel(target, self)
            await self.core.handle_event(IRCNoticeChannelEvent(message, chan, user, self))
        else:
            await self.core.handle_event(IRCNoticeUserEvent(message, user, self))


class CipherClient(pydle.Client):
    RECONNECT_ON_ERROR = False

    def __init__(self, conn):
        self.conn = conn
        super().__init__(self.conn.c.nickname, username=self.conn.c.username, realname=self.conn.c.realname,
                         eventloop=self.conn.loop)

    async def connect(self):
        channels = [c['name'] for c in self.conn.c.channels]
        await super().connect(hostname=self.conn.c.host, port=self.conn.c.port, channels=channels,
                              password=self.conn.c.password, tls=self.conn.c.ssl, tls_verify=self.conn.c.ssl_verify)

    async def on_connect(self):
        await super().on_connect()
        await self.conn.on_connect()

    async def on_disconnect(self, expected):
        await super().on_disconnect(expected)
        await self.conn.on_disconnect(expected)

    async def on_message(self, target, sender, message):
        await super().on_message(target, sender, message)
        await self.conn.on_message(target, sender, message)

    async def on_notice(self, target, sender, message):
        await super().on_notice(target, sender, message)
        await self.conn.on_notice(target, sender, message)

    async def on_ctcp_action(self, sender, target, message):
        await self.conn.on_action(sender, target, message)

    async def on_join(self, user, channel):
        await super().on_join(user, channel)
        await self.conn.on_join(user, channel)

    async def on_part(self, user, channel, message=None):
        await super().on_part(user, channel, message)
        await self.conn.on_part(user, channel, message)

    async def on_quit(self, user, message=None):
        await super().on_quit(user, message)
        await self.conn.on_quit(user, message)
